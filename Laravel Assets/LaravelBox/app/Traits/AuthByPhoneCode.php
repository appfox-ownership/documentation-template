<?php

namespace App\Traits;

use App\Helpers\Functions;
use App\Http\Requests\LoginByPhoneCodeRequest;
use App\Http\Requests\RegisterByPhoneCodeRequest;
use App\Http\Resources\UserResource;
use App\Models\TempPassword;
use App\Models\User;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait AuthByPhoneCode
{
    // 1 сутки
    protected int $codeLifeTime = 24 * 60 * 60;

    protected int $maxAttemptsCount = 3;

    public function login(LoginByPhoneCodeRequest $request): UserResource|Response|JsonResponse
    {
        if(!User::query()->where('phone', $request->get('phone'))->exists()) {
            return response()->json([
                'message' => "User not founded"
            ], 401);
        }

        if(
            $request->has('code') &&
            TempPassword::query()->where('phone', $request->get('phone'))
                ->where('attempt', '<=', 3)
                ->where('end_time', '>=', now())
                ->exists()
        ) {
            return $this->confirmLogin($request);
        }

        $this->generateTempPassword($request->get('phone'));

        return response()->noContent();
    }

    private function generateTempPassword($phone): Response|JsonResponse
    {
        $tempPassword = TempPassword::query()->where('phone', $phone)->first();

        if(!isset($tempPassword)) {
            $code = Functions::generateCode(config('app.debug'));

            TempPassword::query()->create([
                'phone' => $phone,
                'temp_code' => $code,
                'end_time' => now()->addSeconds($this->codeLifeTime),
            ]);
        } else if($tempPassword->attempts < $this->maxAttemptsCount && $tempPassword->end_time < now()) {
            $code = Functions::generateCode(config('app.debug'));
            $tempPassword->update([
                'temp_code' => $code,
                'end_time' => now()->addSeconds($this->codeLifeTime),
                'attempt' => $tempPassword->attempt++
            ]);
        } else {
            return response()->json([
                'message' => "Code already generated"
            ], 401);
        }

        Functions::sendSms($phone, $code);

        return response()->noContent();
    }

    private function confirmLogin(LoginByPhoneCodeRequest $request): UserResource|JsonResponse
    {
        $tempPassword = TempPassword::query()->where('phone', $request->get('phone'))
            ->where('temp_code', $request->get('code'))
            ->firstOrFail();

        if ($tempPassword->end_time < now()) {
            return response()->json([
                'message' => "Expired code lifetime"
            ], 401);
        }

        $user = User::query()->where('phone', $request->get('phone'))->firstOrFail();

        $user->tokens()->delete();
        $user->api_token = $user->createToken($request->get('phone'), ['role:user'])->plainTextToken;

        $tempPassword->delete();

        return UserResource::make($user);
    }

    public function register(RegisterByPhoneCodeRequest $request): Response|JsonResponse
    {
        $hasExistCredentials = User::query()->where('phone', $request->get('phone'))->exists();
        if($hasExistCredentials) {
            return response()->json([
                'message' => "Phone already registered"
            ], 401);
        }

        User::query()->create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' => Hash::make(md5(time()))
        ]);

        $this->generateTempPassword($request->get('phone'));

        return response()->noContent();
    }
}
