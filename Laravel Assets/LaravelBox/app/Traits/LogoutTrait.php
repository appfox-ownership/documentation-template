<?php

namespace App\Traits;

use App\Models\User;

trait LogoutTrait
{
    public function logout(User $user)
    {
        $user->tokens()->delete();
    }
}
