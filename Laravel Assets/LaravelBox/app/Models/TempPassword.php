<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TempPassword
 *
 * @property int $id
 * @property string $phone
 * @property string $temp_code;
 * @property \Illuminate\Support\Carbon|null $end_time
 * @property int $attempt
 * @method static \Illuminate\Database\Eloquent\Builder|TempPassword newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TempPassword newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TempPassword query()
 * @mixin \Eloquent
 */
class TempPassword extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone',
        'temp_code',
        'end_time',
        'attempt'
    ];
}
