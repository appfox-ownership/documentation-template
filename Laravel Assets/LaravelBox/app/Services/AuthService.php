<?php

namespace App\Services;

use App\Traits\AuthByEmailPassword;
use App\Traits\AuthByPhoneCode;
use App\Traits\LogoutTrait;

class AuthService
{
    use AuthByPhoneCode, LogoutTrait;

    public function __construct()
    {
        $this->authField = 'email';
    }
}
