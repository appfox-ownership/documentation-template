<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;

class UserService
{
    protected User $user;

    public function __construct(User $user) {
        $this->user = $user;
    }

    public function me(): UserResource
    {
        return UserResource::make($this->user);
    }
}
