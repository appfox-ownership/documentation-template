<?php

namespace App\Helpers;

class Functions
{
    public static function sendSms($phone, $message) {
        // Code here
    }

    public static function generateCode($isDev = false): string
    {
        if($isDev) {
            return '11111';
        }

        return substr(base_convert(sha1(uniqid(mt_rand(), true)), 16, 36), 0, 5);
    }
}
