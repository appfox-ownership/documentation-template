export const camelCaseParser = function (nameOfProp) {
  const upperCasePos = [];
  let newString = nameOfProp;
  for (let i = 0; i < nameOfProp.length; i++) {
    if (nameOfProp[i] !== nameOfProp[i].toLowerCase()) {
      upperCasePos.push(i);
    }
  }
  for (let i = 0; i < upperCasePos.length; i++) {
    newString =
      newString.split(nameOfProp[upperCasePos[i]])[0] +
      "_" +
      nameOfProp[upperCasePos[i]].toLowerCase() +
      nameOfProp.split(nameOfProp[upperCasePos[i]])[1];
  }
  return newString;
}
