export default {
  state: {
    users: [
      {
        id: "1234567890",
        login: "Kalaronit",
        name: "Владимир",
        email: "Example@gmail.com",
        role: "Админ",
        status: "Не забанен",
        comment: "123",
        note: "3210313ksd",
      },
      {
        id: "1234567891",
        login: "Kalaronit",
        name: "Владимир",
        email: "Example@gmail.com",
        role: "Админ",
        status: "Не забанен",
        comment: "123",
        note: "3210313ksd",
      },
      {
        id: "1234567892",
        login: "Kalaronit",
        name: "Владимир",
        email: "Example@gmail.com",
        role: "Админ",
        status: "Не забанен",
        comment: "123",
        note: "3210313ksd",
      },
      {
        id: "1234567893",
        login: "Kalaronit",
        name: "Владимир",
        email: "Example@gmail.com",
        role: "Админ",
        status: "Не забанен",
        comment: "123",
        note: "3210313ksd",
      },
      {
        id: "1234567894",
        login: "Kalaronit",
        name: "Владимир",
        email: "Example@gmail.com",
        role: "Админ",
        status: "Не забанен",
        comment: "123",
        note: "3210313ksd",
      },
      {
        id: "1234567895",
        login: "Kalaronit",
        name: "Владимир",
        email: "Example@gmail.com",
        role: "Админ",
        status: "Не забанен",
        comment: "123",
        note: "3210313ksd",
      },
    ],
  },

  mutations: {
    ADD_USER(state, payload) {
      state.users.push(payload);
    },

    BAN_USER(state, payload) {
      state.users.splice(payload, 1);
    },

    EDIT_USER(state, payload) {
      state.users[state.users.findIndex(el => el.id === payload.id)] = payload;
    }
  },

  actions: {
    addUser({
      commit
    }, user) {
      commit("ADD_USER", user);
    },

    banUser({
      commit
    }, id) {
      commit("BAN_USER", id);
    },

    editUser({
      commit
    }, user) {
      commit("EDIT_USER", user);
    }
  },

  getters: {
    getUserById: state => state.users.reduce((out, item) => {
      out[item.id] = item;
      return out;
    }, {}),
  },
}
