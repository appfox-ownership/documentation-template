/**
 * Форматирует дату в dd.MM.yyyy
 *
 * @param   {Date}  date  - объект класса Date
 * @param   {Boolean}  isUTC  - флаг перевода времени
 *
 * @return  {String}        дата в формате dd.MM.yyyy
 */
export const formatDate = function (date, isUTC = false) {
  let dd = isUTC ? date.getUTCDate() : date.getDate();
  if (dd < 10) dd = '0' + dd;

  let mm = isUTC ? date.getUTCMonth() + 1 : date.getMonth() + 1; // месяц 1-12
  if (mm < 10) mm = '0' + mm;

  let yyyy = isUTC ? date.getUTCFullYear() : date.getFullYear();
  while (yyyy.length < 4) yyyy.unshift('0');

  return dd + '.' + mm + '.' + yyyy;
}

/**
 * Форматирует дату в dd.MM.yyyy hh:mm
 *
 * @param   {Date}  date  - объект класса Date
 * @param   {Boolean}  isUTC  - флаг перевода времени
 *
 * @return  {String}        дата в формате dd.MM.yyyy hh:mm
 */
export const formatDateAndTime = function (date, isUTC = false) {
  return formatDate(date, isUTC) + ' ' + getTime(date, isUTC);
}

/**
 * Возвращает время в формате hh:mm
 *
 * @param   {Date}  date  - объект класса Date
 * @param   {Boolean}  isUTC  - флаг перевода времени
 *
 * @return  {String}        время в формате hh:mm
 */
export const getTime = function (date, isUTC = false) {
  let hh = isUTC ? date.getUTCHours() : date.getHours();
  if (hh < 10) hh = '0' + hh;

  let mm = isUTC ? date.getUTCMinutes() : date.getMinutes();
  if (mm < 10) mm = '0' + mm;

  return hh + ':' + mm;
}

/**
 * Сравнение двух дней без учета времени
 *
 * @param   {Date}  dateOne  Первая дата
 * @param   {Date}  dateTwo  Вторая дата
 *
 * @return  {Number}           1 если больше, -1 если меньше, 0 если равны
 */
export const compareDays = (dateOne, dateTwo) => {
  const dateA = new Date(
    dateOne.getYear(),
    dateOne.getMonth(),
    dateOne.getDay()
  )
  const dateB = new Date(
    dateTwo.getYear(),
    dateTwo.getMonth(),
    dateTwo.getDay()
  )

  if (dateA.getTime() > dateB.getTime()) {
    return 1
  }

  if (dateA.getTime() < dateB.getTime()) {
    return -1
  }

  return 0
}
