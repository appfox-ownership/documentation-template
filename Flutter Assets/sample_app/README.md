# Назначение репозитория

В данном репозитории представлена примерная структура Flutter проекта. 
Основной целью этого репозитория является создания единой структуры, для проектов компании созданных с помощью фреймфорка Flutter. 
Данная структура должна легко маштабироваться и покрываться тестами, при этом не требовать написания огромного количества повторяющегося кода. Также она должна быть понятной для более легкой релокации программистов компании между проектами.
При написании кода рекомендуется активно использовать кодогенерацию.

## Структура проекта

В проектах компании планируется использовать следующие пакеты:
  flutter_bloc: any
  flutter_svg: any
  freezed: any
  freezed_annotation: any
  json_annotation: any
  json_serializable: any
  http: any
  build_runner: any
  flutter_lints: any

Весь проект разделен на несколько слоев. Каждый слой распологается в соответсвующей директории. В каждой директории находится файл для экспорта зависимостей с названием текущей директории. Разберем подробнее каждый слой. 

### Core

Не является, как таковым слоем приложения. В данной директории расположены глобальные файлы, используемые в приложении.
К примеру здесь содержатся файлы с темой, текстовыми стилями, маршрутами, enums, extensions и т.д. Особое внимание хотелось бы акцентировать на директории utils, в которой расположены файлы HttpClient, HttpResponse и UseCaseResponse, которые играю основную роль в данной архитектуре. Принцип их использования будет подробно описан ниже. 

### Models

Слой состоит из классов, которые определяют сущности, используемые в данном проекте. Разделения на Entity и DTO, на мой взгляд, здесь не требуется. При создании каждого класса рекомендуется использовать кодогенерацию.

### Repositories 

Слой предназначен для получения данных из внешних источников. Для каждой фичи создается соответсвующий репозиторий. Каждый репозиторий содержит несколько асинхронных методов, которые возвращают объект HttpResponse.

### UseCases 

Слой предназначен для обработки полученных данных. Для каждой фичи создается соответсвующий файл. Каждый метод UseCase возвращает объект UseCaseResponse.

### Presentation

Слой состоит из 3 директорий. Директории bloc и screens не требуют пояснений. В директории widgets находятся виджеты, которые переиспользуются во всем приложении, к примеру кнопки, чекбоксы и т.д.

## Пояснение архитектуры

Разберем принцип работы данной архитектуры на примере.
Допустим пользователь хочет получить новый массив данных при нажатии на кнопку.

Схематично данную структуру можно изобразить следующим образом:

Screen -> Bloc -> Usecase -> Repository -> DataService

Разберем подробнее каждый этап:

1) При нажатии на кнопку вызывается event соответсвующего Bloc.

2) Bloc, получив новый ивент, вызывает определенный метод соотвутсвующего Usecase.

3) В Usecase вызывается метод репозитория, отвечающего за получение требуемых данных.

4) В репозитории(см. /lib/repositories/sample_repository.dart) объявляется httpClient(см. /lib/core/utils/http_client.dart), методы которого вызываются для получения данных. В зависимости от результата выполнения http запроса из репозитория в UseCase возвращается соответствующий объект HttpResponse(см. /lib/core/utils/http_response.dart).

5) В Usecase(см. /lib/usecases/sample_usecase.dart) обрабатывается полученный объект HttpResponse и в зависимости от его типа и параметров создается объект UseCaseResponse(см. /lib/core/utils/usecase_response.dart), который возвращается в Bloc.

6) В Bloc(см. /lib/presentation/bloc/feature1/sample1_bloc.dart) в зависимости от полученного объекта UseCaseResponse возвращается соответсвующий state.
P.s. можно пренебречь слоем UseCase если не требуется обрабатывать полученные данные. В таком случае в Bloc вызывается метод репозитория.

7) В соответсвии со вернувшимся state отображается необходимый UI.

