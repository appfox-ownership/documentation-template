// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'usecase_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UseCaseResponse {
  dynamic get data => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic data) success,
    required TResult Function(dynamic data) fail,
    required TResult Function(dynamic data) other,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(dynamic data)? success,
    TResult Function(dynamic data)? fail,
    TResult Function(dynamic data)? other,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic data)? success,
    TResult Function(dynamic data)? fail,
    TResult Function(dynamic data)? other,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Success value) success,
    required TResult Function(Fail value) fail,
    required TResult Function(Other value) other,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Success value)? success,
    TResult Function(Fail value)? fail,
    TResult Function(Other value)? other,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Success value)? success,
    TResult Function(Fail value)? fail,
    TResult Function(Other value)? other,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UseCaseResponseCopyWith<UseCaseResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UseCaseResponseCopyWith<$Res> {
  factory $UseCaseResponseCopyWith(
          UseCaseResponse value, $Res Function(UseCaseResponse) then) =
      _$UseCaseResponseCopyWithImpl<$Res>;
  $Res call({dynamic data});
}

/// @nodoc
class _$UseCaseResponseCopyWithImpl<$Res>
    implements $UseCaseResponseCopyWith<$Res> {
  _$UseCaseResponseCopyWithImpl(this._value, this._then);

  final UseCaseResponse _value;
  // ignore: unused_field
  final $Res Function(UseCaseResponse) _then;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      data: data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
abstract class _$$SuccessCopyWith<$Res>
    implements $UseCaseResponseCopyWith<$Res> {
  factory _$$SuccessCopyWith(_$Success value, $Res Function(_$Success) then) =
      __$$SuccessCopyWithImpl<$Res>;
  @override
  $Res call({dynamic data});
}

/// @nodoc
class __$$SuccessCopyWithImpl<$Res> extends _$UseCaseResponseCopyWithImpl<$Res>
    implements _$$SuccessCopyWith<$Res> {
  __$$SuccessCopyWithImpl(_$Success _value, $Res Function(_$Success) _then)
      : super(_value, (v) => _then(v as _$Success));

  @override
  _$Success get _value => super._value as _$Success;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_$Success(
      data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$Success implements Success {
  const _$Success(this.data);

  @override
  final dynamic data;

  @override
  String toString() {
    return 'UseCaseResponse.success(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Success &&
            const DeepCollectionEquality().equals(other.data, data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(data));

  @JsonKey(ignore: true)
  @override
  _$$SuccessCopyWith<_$Success> get copyWith =>
      __$$SuccessCopyWithImpl<_$Success>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic data) success,
    required TResult Function(dynamic data) fail,
    required TResult Function(dynamic data) other,
  }) {
    return success(data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(dynamic data)? success,
    TResult Function(dynamic data)? fail,
    TResult Function(dynamic data)? other,
  }) {
    return success?.call(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic data)? success,
    TResult Function(dynamic data)? fail,
    TResult Function(dynamic data)? other,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Success value) success,
    required TResult Function(Fail value) fail,
    required TResult Function(Other value) other,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Success value)? success,
    TResult Function(Fail value)? fail,
    TResult Function(Other value)? other,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Success value)? success,
    TResult Function(Fail value)? fail,
    TResult Function(Other value)? other,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class Success implements UseCaseResponse {
  const factory Success(final dynamic data) = _$Success;

  @override
  dynamic get data;
  @override
  @JsonKey(ignore: true)
  _$$SuccessCopyWith<_$Success> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FailCopyWith<$Res> implements $UseCaseResponseCopyWith<$Res> {
  factory _$$FailCopyWith(_$Fail value, $Res Function(_$Fail) then) =
      __$$FailCopyWithImpl<$Res>;
  @override
  $Res call({dynamic data});
}

/// @nodoc
class __$$FailCopyWithImpl<$Res> extends _$UseCaseResponseCopyWithImpl<$Res>
    implements _$$FailCopyWith<$Res> {
  __$$FailCopyWithImpl(_$Fail _value, $Res Function(_$Fail) _then)
      : super(_value, (v) => _then(v as _$Fail));

  @override
  _$Fail get _value => super._value as _$Fail;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_$Fail(
      data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$Fail implements Fail {
  const _$Fail(this.data);

  @override
  final dynamic data;

  @override
  String toString() {
    return 'UseCaseResponse.fail(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Fail &&
            const DeepCollectionEquality().equals(other.data, data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(data));

  @JsonKey(ignore: true)
  @override
  _$$FailCopyWith<_$Fail> get copyWith =>
      __$$FailCopyWithImpl<_$Fail>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic data) success,
    required TResult Function(dynamic data) fail,
    required TResult Function(dynamic data) other,
  }) {
    return fail(data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(dynamic data)? success,
    TResult Function(dynamic data)? fail,
    TResult Function(dynamic data)? other,
  }) {
    return fail?.call(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic data)? success,
    TResult Function(dynamic data)? fail,
    TResult Function(dynamic data)? other,
    required TResult orElse(),
  }) {
    if (fail != null) {
      return fail(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Success value) success,
    required TResult Function(Fail value) fail,
    required TResult Function(Other value) other,
  }) {
    return fail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Success value)? success,
    TResult Function(Fail value)? fail,
    TResult Function(Other value)? other,
  }) {
    return fail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Success value)? success,
    TResult Function(Fail value)? fail,
    TResult Function(Other value)? other,
    required TResult orElse(),
  }) {
    if (fail != null) {
      return fail(this);
    }
    return orElse();
  }
}

abstract class Fail implements UseCaseResponse {
  const factory Fail(final dynamic data) = _$Fail;

  @override
  dynamic get data;
  @override
  @JsonKey(ignore: true)
  _$$FailCopyWith<_$Fail> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$OtherCopyWith<$Res>
    implements $UseCaseResponseCopyWith<$Res> {
  factory _$$OtherCopyWith(_$Other value, $Res Function(_$Other) then) =
      __$$OtherCopyWithImpl<$Res>;
  @override
  $Res call({dynamic data});
}

/// @nodoc
class __$$OtherCopyWithImpl<$Res> extends _$UseCaseResponseCopyWithImpl<$Res>
    implements _$$OtherCopyWith<$Res> {
  __$$OtherCopyWithImpl(_$Other _value, $Res Function(_$Other) _then)
      : super(_value, (v) => _then(v as _$Other));

  @override
  _$Other get _value => super._value as _$Other;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_$Other(
      data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$Other implements Other {
  const _$Other(this.data);

  @override
  final dynamic data;

  @override
  String toString() {
    return 'UseCaseResponse.other(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Other &&
            const DeepCollectionEquality().equals(other.data, data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(data));

  @JsonKey(ignore: true)
  @override
  _$$OtherCopyWith<_$Other> get copyWith =>
      __$$OtherCopyWithImpl<_$Other>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic data) success,
    required TResult Function(dynamic data) fail,
    required TResult Function(dynamic data) other,
  }) {
    return other(data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(dynamic data)? success,
    TResult Function(dynamic data)? fail,
    TResult Function(dynamic data)? other,
  }) {
    return other?.call(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic data)? success,
    TResult Function(dynamic data)? fail,
    TResult Function(dynamic data)? other,
    required TResult orElse(),
  }) {
    if (other != null) {
      return other(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Success value) success,
    required TResult Function(Fail value) fail,
    required TResult Function(Other value) other,
  }) {
    return other(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Success value)? success,
    TResult Function(Fail value)? fail,
    TResult Function(Other value)? other,
  }) {
    return other?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Success value)? success,
    TResult Function(Fail value)? fail,
    TResult Function(Other value)? other,
    required TResult orElse(),
  }) {
    if (other != null) {
      return other(this);
    }
    return orElse();
  }
}

abstract class Other implements UseCaseResponse {
  const factory Other(final dynamic data) = _$Other;

  @override
  dynamic get data;
  @override
  @JsonKey(ignore: true)
  _$$OtherCopyWith<_$Other> get copyWith => throw _privateConstructorUsedError;
}
