<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

Данный пакет представляет собой загрузочный экран с логотипом AppFox.

## Getting started

Добавьте пакет в pubspec.yaml:

```
dependencies:
  ...
  appfox_splash:   
    git:
      url: https://gitlab.com/Igor_Maicovich/appfox_splash.git
  ...
```  

## Usage

Добавьте виджет AppFoxSplash() в Ваше приложение вместо загрузочного экрана
```
class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AppFoxSplash(),
    );
  }
}
```
## Additional information

#### Свойства AppFoxSplash():
  * child → Widget?
  * color → Color?
  * decoration → Decoration?
  * foregroundDecoration → Decoration?
  * padding → EdgeInsetsGeometry?

Свойства аналогичны свойствам Container()   
https://api.flutter.dev/flutter/widgets/Container-class.html

Для создания загрузочного экрана с логотипом заказчика 
заполните свойство child виджетом с этим логотипом.
```
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AppFoxSplash(
        child:Logo(),
      ),
    );
  }
}
```

Для создания загрузочного экрана с логотипом AppFox
оставьте свойство child пустым.
