import 'package:appfox_splash/src/widgets/src.dart';
import 'package:flutter/material.dart';

class AppFoxSplash extends StatelessWidget {
  final EdgeInsetsGeometry? padding;
  final Decoration? decoration;
  final Decoration? foregroundDecoration;
  final Color? color;
  final Widget? child;
  const AppFoxSplash({
    Key? key,
    this.padding = EdgeInsets.zero,
    this.decoration,
    this.foregroundDecoration,
    this.color,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: padding,
        decoration: decoration,
        foregroundDecoration: foregroundDecoration,
        color: color,
        child: child == null ? const AppFoxCenter() : AppFoxMini(child: child!),
      ),
    );
  }
}
