import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppFoxMini extends StatefulWidget {
  final Widget child;
  const AppFoxMini({Key? key, required this.child}) : super(key: key);

  @override
  State<AppFoxMini> createState() => _AppFoxMiniState();
}

class _AppFoxMiniState extends State<AppFoxMini> {
  late double size;

  @override
  void didChangeDependencies() {
    var query = MediaQuery.of(context);
    size = query.orientation == Orientation.portrait ? query.size.height : query.size.width;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(child: widget.child),
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          child: SizedBox(
            height: size / 20,
            width: size / 20,
            child: SvgPicture.asset(
              'packages/appfox_splash/lib/src/assets/appfox_mini.svg',
              fit: BoxFit.fitHeight,
            ),
          ),
        ),
      ],
    );
  }
}
