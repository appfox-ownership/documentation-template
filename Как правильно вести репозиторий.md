# Требования ведения репозитория

## Задачи/Ветки

Каждая задача должна быть декомпозирована и на доске не должно быть задач, на которые бы уходило больше недели. Если на задачу уходит больше 40 часов, значит задача недостаточно декомпозирована.
На каждую задачу
На каждую большую задачу или на группу маленьких задач создаем отдельную **ветку**. 

Именование: **НомерЗадачиНаДоске-ИмяРазработчика-КраткоеОписаниеЗадачи**

Пример: **123-Timur-MainScreen**


## Коммиты
Коммиты оформлять следующим образом:

Именование: **НомерЗадачиНаДоске-ИмяРазработчика-ОписаниеКоммита**

Пример: **123-Timur- Added MainScreen.cs**

Если описание коммита большое или нужно уточнение, то заполняем колоночку "Description".

## Иерархия веток
В проекте должны существовать следующие ветки:
1) Главная(релизная) ветка -- **Main** или **Maser**. Далее **Main**
Находясь на этой ветке мы должны делать публикацию.
2) Пред-релизная ветка -- **Stage**
Ветка для проведения предварительных тестов.
3) Ветка разработки -- **Dev**

От нее мы должны ответвляться при создании своей ветки, т.к. ветка **Main** -- самая протестированная.
```mermaid
graph LR
C1((Commit1))
C2((Commit2))
M{Main}
M --> C1
M --> C2
```
Далее работаем над своей задачей. При ее выполнении нам необходимо пройти несколько этапов.  Свою ветку вливаем в **Dev**. 
```mermaid  
    graph LR
D[Dev]
C1((Commit1))
C2((Commit2))
C1 --> D
C2 --> D
```
Если нет конфликтов, то заходим на **Dev** и проводим небольшой тест. Продолжительность теста зависит от величины задачи. От 5-и минут до 1 часа. Если ошибок или багов не обнаружили, то значит можно свою ветку влить в **Stage**.
```mermaid  
    graph LR
S[Stage]
C1((Commit1))
C2((Commit2))
C1 --> S
C2 --> S
```
 Если обнаружили баг, то нужно разобраться по какой причине в вашей ветке этого бага нет, а в ветке **Dev**, после вливания вашей ветки, он есть.
 
 Каким образом мы устраняем этот баг? 
 
 Найдя причину бага, мы поймем, что он связан с веткой нашего коллеги. Поэтому его ветку необходимо будет влить в вашу ветку. Или наоборот, если принято решение, что этот баг будет устранять ваш коллега.
```mermaid  
    graph LR
S[Stage]
C1((Commit1))
C2((Commit2))
C2 --> C1
```
После устранения бага необходимо влить исправленную ветку в ветку **Stage**
```mermaid  
    graph LR
S[Stage]
C1((Commit1))
C2((Commit2))
C2 --> S
```
Когда все необходимые задачи влиты и принято решение делать релиз, то тестировщик или тестировщики тестируют **Stage**. Во время теста запрещается вливать в **Stage** другие ветки.

Если находится баг, то задача возвращается обратно на разработку. При этом на нее ставится высокий приоритет, т.к. на нее уже потрачено время и стоит запрет на вливание в **Stage**. Разработчик должен перейти на эту задачу, даже если он сейчас уже задействован на другой задаче. В этот момент вся команда должна быть заинтересована в выполнении этой задачи. Т.к. **Stage** заблокирован.

Задача, при возвращении в разработку, должна будет пройти все этапы заново, для того, чтобы снова попасть в **Stage**.

Как только **Stage** протестирован. Вливаем **Stage** в **Main** и делаем публикацию, которую тоже должны протестировать.
```mermaid  
    graph LR
S[Stage]
M{Main}
S --> M
```
Если так же находится баг, то повторяем этап.

## Решение конфликтов
При вливании ветки в **Dev** или **Stage** могут возникнуть конфликты, которые необходимо решить.

При возникновении конфликта необходимо найти задачу, с которой конфликт этот возник и влить эту ветку, связанную с этой задачей, в свою ветку. Таким образом мы обеспечим максимальную чистоту веток. После решения конфликта заново заливаем свою ветку в какую планировали.

Обратите внимание, что если при вливании в **Dev** конфликта не было, а при вливании в **Stage** он появился, то значит кто-то нарушил рабочий цикл.

## Итог
Такой подход помогает достичь максимального качества продукта на этапе публикации, моментально реагировать на слабые точки проекта, что ускоряет процесс публикации.
